import React from 'react';
import { BottomNavigation} from 'react-native-paper';
import { Platform,StyleSheet} from 'react-native';

import Home from '../components/organisms/home/Home';
import MessageScreen from '../components/organisms/messages/MessageScreen';
import SearchScreen from '../components/organisms/search/SearchScreen';
import Settings from '../components/organisms/settings/Settings'
import MyTaskTopTabNavigator from './MyTaskTopTabNavigator';

import {PRIMARY} from '../styles/colors'


        
const BottomTabNavigator = (props) => {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'home', title: 'Home', icon: 'home', color:PRIMARY},
    { key: 'myTask', title: 'My Task', icon: 'hammer', color:PRIMARY },
    { key: 'search', title: 'Search', icon: 'magnify' ,color: PRIMARY},
    { key: 'message', title: 'Messages', icon: 'email', color: PRIMARY},
    { key: 'settings', title: 'Settings', icon:'settings' ,color: PRIMARY }
  ]);

  const renderScene = BottomNavigation.SceneMap({
    home: Home,
    //myTask:MyTask,
    myTask:MyTaskTopTabNavigator,
    message: MessageScreen,
    search: SearchScreen,
    settings: Settings
  });
  return (
    <>
      <BottomNavigation
        navigationState={{ index, routes }}
        onIndexChange={setIndex}
        renderScene={renderScene}
        barStyle = {Platform.OS === "ios" ? {height:70} : {height:BottomNavigation.currentHeight}}
        //navigation={props.navigation}
      />
   
    </>
  );
};

export default BottomTabNavigator;

const styles = StyleSheet.create({
  title:{
    marginTop:10,
  },
  
});
