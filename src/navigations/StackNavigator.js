import EnterEmail from '../components/organisms/onBoarding/EnterEmail'
import Login from '../components/organisms/onBoarding/Login'
//import Register from '../components/organisms/onBoarding/Register'
//import RegisterChoice from '../components/organisms/onBoarding/RegisterChoice'
import Verify from '../components/organisms/onBoarding/Verify'
import PasswordCreate from '../components/organisms/onBoarding/PasswordCreate';
import BottomTabNavigator from './BottomTabNavigator';
import { createStackNavigator } from '@react-navigation/stack';

import React from 'react'
//import { Platform } from 'react-native'
const Stack = createStackNavigator();

const StackNavigator = () => {
   
    return (
    
        <Stack.Navigator
            initialRouteName="RegisterChoice"
            screenOptions={{ 
                gestureEnabled: false,
                headerTransparent:'true', 
                headerTitleStyle: {
                    fontWeight: 'bold',
                    alignSelf:'center'
                },
            }}
        >
                {/* <Stack.Screen
                    name="RegisterChoice"
                    component={RegisterChoice}
                    options={{ 
                        title: 'Options',
                        
                    }}
                /> */}
                <Stack.Screen
                    name="EnterEmail"
                    component={EnterEmail}
                    options={{ title: 'Enter Email' }}
                />
                <Stack.Screen
                    name="Login"
                    component={Login}
                    options={{ title: 'Login' }}
                />
                <Stack.Screen
                    name="Verify"
                    component={Verify}
                    options={{ title: 'Verify Password' }}
                />
                <Stack.Screen
                    name="PasswordCreate"
                    component={PasswordCreate}
                    options={{ title: 'Password Create' }}
                    //initialParams={{ user: 'me' }}
                />
                <Stack.Screen
                    name="BottomTabNavigator"
                    component={BottomTabNavigator}
                    options={{ title: '',headerLeft:null }}
                    initialParams={{ user: 'me' }}
                />
        </Stack.Navigator>
    
 );
}

export default StackNavigator
