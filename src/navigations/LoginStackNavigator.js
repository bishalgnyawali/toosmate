
import Login from '../components/organisms/login/LogIn';
import BottomTabNavigator from '../navigations/BottomTabNavigator';
//import Register from '../components/organisms/onBoarding/Register'
import { createStackNavigator } from '@react-navigation/stack';

import React from 'react'
import { Platform } from 'react-native'
const Stack = createStackNavigator();
const LoginStackNavigator = () => {
    
    return (
    
        <Stack.Navigator
                initialRouteName="Login"
                //headerMode="none"
                screenOptions={{ 
                gestureEnabled: false,
                headerTransparent:'true', 
                headerTitleStyle: {
                fontWeight: 'bold',
                alignSelf:'center'
            },
         }}
        >
                
                
                <Stack.Screen
                    name="Login"
                    component={Login}
                    options={
                        { 
                            title: '',
                            headerTitleStyle: {
                                color:'black',
                                //fontFamily: Platform.OS === 'ios' ?'Noteworthy-Bold':'sans-serif-thin',
                                fontSize:30,
                                alignSelf:'center'
                            },
                        }}
                />
                <Stack.Screen
                    name="BottomTabNavigator"
                    component={BottomTabNavigator}
                    options={{ title: '',headerLeft:null }}
                    initialParams={{ user: 'me' }}
                />
                
        </Stack.Navigator>
);
}

export default LoginStackNavigator
