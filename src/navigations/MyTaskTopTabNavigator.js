import React from 'react';
//import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';


import Tasks from '../components/organisms/myTask/Tasks'
import Booking from '../components/organisms/myTask/Booking'
//import { HeaderTitle } from '@react-navigation/stack';
import Common from '../components/organisms/home/iconNavigation/Common'
import {PRIMARY,WHITE} from '../styles/colors'


export default function MyTaskTopTabNavigator() {

    const Tab = createMaterialTopTabNavigator();
    //const insets = useSafeArea();
    //useEf

    return (
        <>
            <Common name="My Tasks"/>
            <Tab.Navigator 
                tabBarOptions={{
                    activeTintColor: WHITE,
                    tabStyle:{
                        backgroundColor:PRIMARY,
                    },
                    style: {
                        // marginTop: insets.top+40,
                        borderTopWidth: 0,
                        //borderTopColor: "transparent",
                       
                    }
                }}
                //swipeEnabled={false}
                //backBehavior={"initialRoute"}
            >
                <Tab.Screen name="Tasks" component={Tasks}/>
                <Tab.Screen name="Booking" component={Booking}/>
                
            </Tab.Navigator>
        </>
    );
}
