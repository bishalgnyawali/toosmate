import React from 'react';
//import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import Description from '../components/organisms/home/iconNavigation/Description.js'
import Schedule from '../components/organisms/home/iconNavigation/Schedule.js'
import Budget from '../components/organisms/home/iconNavigation/Budget.js'
//import { HeaderTitle } from '@react-navigation/stack';
import Common from '../components/organisms/home/iconNavigation/Common'
export default function IconStackNavigator() {

    const Tab = createMaterialTopTabNavigator();
    //const insets = useSafeArea();
    //useEf

    return (
        <>
            <Common name="none"/>
            <Tab.Navigator tabBarOptions={{
                style: {
                    // marginTop: insets.top+40,
                    //backgroundColor:'pink',
                        headerStyle: {
                            backgroundColor: 'black',
                        },
                    }
                }}
                swipeEnabled={false}
                //backBehavior={"initialRoute"}
            >
                <Tab.Screen name="Description" component={Description}/>
                <Tab.Screen name="Schedule" component={Schedule}/>
                <Tab.Screen name="Budget" component={Budget}/>
            </Tab.Navigator>
        </>
    );
}
