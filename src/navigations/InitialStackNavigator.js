
//import Login from '../components/organisms/login/LogIn';
import BottomTabNavigator from './BottomTabNavigator';
import Register from '../components/organisms/onBoarding/Register'
import { createStackNavigator } from '@react-navigation/stack';
//import AsyncStorage from '@react-native-community/async-storage';

import React, {useContext} from 'react'
import { Platform } from 'react-native'

import {LoginConsumer} from '../context/LogInContext'
import LoginStackNavigator from './LoginStackNavigator';
import StackNavigator from './StackNavigator';
import IconStackNavigator from './IconStackNavigator'
import AboutToosMate from '../components/organisms/settings/settingComponents/AboutToosMate'
import Help from '../components/organisms/settings/settingComponents/Help'
import MyProfile from '../components/organisms/settings/settingComponents/MyProfile'
import Notifications from '../components/organisms/settings/settingComponents/Nofitfications'
import LoadingActivityPage from '../components/molecules/LoadingActivityPage'
import {LoginContext} from '../context/LogInContext'
import PaymentOptionsTabNavigator from '../components/organisms/settings/settingNavigators/PaymentOptionsTabNavigator'

const Stack = createStackNavigator();

const InitialStackNavigator = () => {
    
    const [login,setLogin]=useContext(LoginContext);//from loginContext
    console.log('loginStatus=== ',login.loginStatus);
    const FirstStack=()=>{
        return(
        
            <Stack.Navigator
                initialRouteName="Register"
                headerMode="none"
                screenOptions={{ 
                    gestureEnabled: false,
                    //headerTransparent:'true', 
                    headerTitleStyle: {
                        fontWeight: 'bold',
                        alignSelf:'center'
                    },
                }}
            >
                <Stack.Screen
                    name="Register"
                    component={Register}
                    options={
                        { 
                            title: '',
                            headerTitleStyle: {
                                //color:'white',
                                fontFamily: Platform.OS === 'ios' ?'Noteworthy-Bold':'sans-serif-thin',
                                fontSize:30,
                                alignSelf:'center'
                            },
                        }}
                />
                
                <Stack.Screen
                    name="LoginStackNavigator"
                    component={LoginStackNavigator}
                    options={
                        { 
                            title: 'Login',
                            headerTitleStyle: {
                                //gcolor:'white',
                                fontFamily: Platform.OS === 'ios' ?'Noteworthy-Bold':'sans-serif-thin',
                                fontSize:30,
                                alignSelf:'center'
                            },
                        }}
                />
                <Stack.Screen
                    name="StackNavigator"
                    component={StackNavigator}
                    options={{ title: '',headerLeft:null }}
                    initialParams={{ user: 'me' }}
                />
                {/* <Stack.Screen
                    name="BottomTabNavigator"
                    component={BottomTabNavigator}
                    options={{ title: '',headerLeft:null }}
                    initialParams={{ user: 'me' }}
                /> */}
                
        </Stack.Navigator>
        
        );
    }//FirstStack arrow function finishes

/********************************************** */

    //this arrow function returns Stack Navigation directly to landing page if user is logged in the device.
    const SecondStack=()=>{
        return(
                <Stack.Navigator
                    initialRouteName="BottomTabNavigator"
                    screenOptions={{ 
                        gestureEnabled: true,
                        headerTransparent:'true', 
                        headerTitleStyle: {
                            fontWeight: 'bold',
                            //alignSelf:'center'
                        },
                        
                    }}
                >
                    <Stack.Screen
                        headerMode="none"
                        name="BottomTabNavigator"
                        component={BottomTabNavigator}
                        options={{ title: '',headerLeft:null,marginTop:20 }}
                        initialParams={{ user: 'me' }}
                    />
                    
                    <Stack.Screen
                        name="IconStackNavigator"
                        component={IconStackNavigator}
                        options={{ 
                            title: '',
                            headerTintColor: 'black',
                        }}
                        swipeEnable={false}
                        initialParams={{ user: 'me' }}
                    />
                    <Stack.Screen
                        name="PaymentOptionsTabNavigator"
                        component={PaymentOptionsTabNavigator}
                        options={{ 
                            title: '',
                            headerTintColor: 'black',
                        }}
                        //swipeEnable={false}
                        initialParams={{ user: 'me' }}
                    />
                    <Stack.Screen
                        name="Help"
                        component={Help}
                        options={{ 
                            title: '',
                            headerTintColor: 'black',
                        }}
                        //swipeEnable={false}
                        initialParams={{ user: 'me' }}
                    />
                    <Stack.Screen
                        name="About"
                        component={AboutToosMate}
                        options={{ 
                            title: '',
                            headerTintColor: 'black',
                        }}
                        //swipeEnable={false}
                        initialParams={{ user: 'me' }}
                    />
                    <Stack.Screen
                        name="Profile"
                        component={MyProfile}
                        options={{ 
                            title: '',
                            headerTintColor: 'black',
                        }}
                        //swipeEnable={false}
                        initialParams={{ user: 'me' }}
                    />
                    <Stack.Screen
                        name="Notifications"
                        component={Notifications}
                        options={{ 
                            title: '',
                            headerTintColor: 'black',
                        }}
                        //swipeEnable={false}
                        initialParams={{ user: 'me' }}
                    />

                    
                </Stack.Navigator>

            
        )
    }//Second Stack function ends

/************************************************************************ */
    //returns StackNavigation component based on user logged in the device, using AsyncStore
    //console.log('\n\n\nbefore return of Initial Component login value ='+login)
    // useEffect(()=>{
    //     if(login.loginStatus instanceof Function)
    //     {
    //         console.log('i am here')
    //         setLogin({
    //             ...login,
    //             loginStatus:true
    //         })
    //     }
    // },[])

const load=()=>{
    if(login.loginStatus instanceof Function){
       return(
            
            <LoadingActivityPage/>
            
        )
    }else{
        return(
        login.loginStatus?(
            <SecondStack/>
        ):
        (
            <FirstStack/>
        ))
    }
}
    
    return (
        <LoginConsumer>
            {load}
            {/* {
                (
                    () =>
                    //console.log('login in inistack '+context.login);
                        if(login.loginStatus instanceof Function){

                        }else{
                            login.loginStatus?(
                                <SecondStack/>
                            ):
                            (
                                <FirstStack/>
                            )
                        }
                )()
            } */}
        </LoginConsumer>
    );
}// initialStackNavigator component ends

export default InitialStackNavigator
