import React from 'react'
import { View, Text,StyleSheet } from 'react-native'

const ErrorText = (props) => {
    return (
        <View>
            <Text style={{...styles.error,...props.style}}>{props.text}</Text>
        </View>
    )
}

export default ErrorText

const styles = StyleSheet.create({
    error:{
        color:'red',
        fontSize:12,
        elevation:30,
        alignSelf:'center'
        
    }
})
