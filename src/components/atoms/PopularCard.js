import React from 'react'
import { StyleSheet,View} from 'react-native'
import {Card,Title, Paragraph} from 'react-native-paper'

const PopularCard = (props) => {
    return (
        <>
            <Card style={styles.card}>
                    {/* <Card.Title title="Card Title" subtitle="Card Subtitle" left={LeftContent} /> */}
                    
                    <Card.Cover style={styles.cardCover} source={{ uri: props.uri }} />
                    <Card.Content style={styles.cardContent}>
                        <View style={styles.titleParagraphView}>
                            <Title>{props.title}</Title>
                            <Paragraph>
                                Lorem ipsum dolor sit amet, consectetur
                            </Paragraph>
                        </View>
                    </Card.Content>
                    {/* <Card.Actions>
                        <Button>Cancel</Button>
                        <Button>Ok</Button>
                    </Card.Actions>  */}
                </Card>
            </>
    )
}

export default PopularCard;

const styles = StyleSheet.create({
        card:{
            borderRadius:20,
            width:230,
            height:260,
            marginRight:18,
            shadowColor:'black',
            shadowOffset:{width:4,height:-1},
            shadowRadius:10,
            shadowOpacity:0.3,
            elevation:1,
        },
        cardCover:{
            height:150,
            resizeMode:'contain',
            width:'100%',
            height:'60%'
        },
        cardContent:{
            paddingLeft:0,
            marginTop:15,
            borderTopEndRadius:50
        },
        titleParagraphView:{
            marginLeft:10

        }
})

