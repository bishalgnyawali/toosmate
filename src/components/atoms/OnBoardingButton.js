import React from 'react'
import { StyleSheet } from 'react-native'
import {Button as PButton} from 'react-native-paper'

const Button = (props) => {
    return (
        <PButton style={{...styles.button}} mode="contained"  icon={props.icon} onPress={props.onPress}>
                {props.children}
        </PButton>
    )
}

export default Button


const styles = StyleSheet.create({
    
    button:{
      height:50,
      width:300,
      margin:10,
      alignSelf:'center',
      backgroundColor:'green',
      borderRadius:10,
      //paddingTop:7,
      justifyContent:'center'
      //flexDirection:'column-reverse',
    }
 
});
