import React from 'react'
import { ImageBackground,StyleSheet,View } from 'react-native'

const BackGroundImage = (props) => {
    return (
        <View style={styles.container}>
            <ImageBackground source={require(${props.uri})} style={styles.image}>
                <Text style={styles.text}>{props.text}</Text>
            </ImageBackground>
        </View>
    )
}

export default BackGroundImage

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column"
      },
      image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center"
      },
      text: {
        color: "grey",
        fontSize: 30,
        fontWeight: "bold"
      }
})
