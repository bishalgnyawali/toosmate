import React from 'react'
import {StyleSheet,View} from 'react-native'
//import {PRIMARY} from '../../styles/colors';
import {Button as PButton} from 'react-native-paper'
//import PortalHost from 'react-native-paper/lib/typescript/src/components/Portal/PortalHost'

const Button = (props) => {
    return (
        <View style={styles.footer}>
            <PButton style={props.style} mode="contained" onPress={props.onPress}
            >
                {props.name}
            </PButton>
        </View>
    )
}

export default Button



const styles = StyleSheet.create({    
    text:{
        color:'white',
        fontWeight:'bold',
        fontSize:20,
        alignSelf:'center',
        paddingTop:5

    },
    footer:{
        position: 'absolute',
        flex:0.1,
        left: 0,
        right: 0,
        bottom:0,
        //backgroundColor:'green',
        flexDirection:'row',
        //height:80,
        justifyContent:'center',
        
        paddingBottom:20

    }
})