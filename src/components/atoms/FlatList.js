import React from 'react'
import { FlatList } from 'react-native'

const FlatList = () => {
    return (
        <FlatList
            data={DATA}
            renderItem={({ item }) => <Item title={item.title} />}
            keyExtractor={item => item.id}
            horizontal='true'
        />
    )
}

export default FlatList
