import React from 'react';
import {StyleSheet,Text } from 'react-native';
import {TextInput} from 'react-native-paper';
import Content from '../../molecules/Content';
import Button from '../../atoms/OnBoardingButton'
import { TouchableOpacity } from 'react-native-gesture-handler';
import ErrorText from '../../atoms/ErrorText'
const Verify = ({route,navigation}) => {
    const [text, setText] = React.useState({
        fname:'',
        lname:'',
        fnameError:false,
        //errorText:'Last name and first name are not valid'
    });
    
    const {parameter}=route.params;
    //const {caption}=route.params;
    //console.log('caption= '+caption);
    console.log('from Verify params = '+parameter.email);
    const _onPress=()=>{
        if(text.fname.length>=1 && text.lname.length>=1)
        {
            
            if(!text.fnameError)
            {
                parameter.fname=text.fname;
                parameter.lname=text.lname;
                //console.log(navigation);
                navigation.navigate('PasswordCreate',{
                    register:parameter
                });
            }
            
        }else{
            setText({
                ...text,
                //errorText:'First Name must be minimum of length 3',
                fnameError:true
            })
        }
    }
    return (
        <>
            <Content>
            </Content>
                <TextInput
                    style={styles.textInput}
                    placeholder='Enter First Name'
                    underlineColor="green"
                    onChangeText={(input)=>setText({
                        ...text,
                        fnameError:false,
                        fname:input
                    })}
                    onSubmitEditing={() => Keyboard.dismiss()}
                    autoCorrect={false}
                    //autoCapitalize='none'
                />
                <TextInput
                    style={styles.textInput}
                    placeholder='Enter Last Name'
                    underlineColor="green"
                    onChangeText={(input)=>setText({
                        ...text,
                        fnameError:false,
                        lname:input
                    })}
                    onSubmitEditing={() => Keyboard.dismiss()}
                    autoCorrect={false}
                    
                />
                {text.fnameError  && <ErrorText text={'First Name and Last Name cannot be empty'}/>}
                <Button style={{backgroundColor:'green'}} icon="update" mode="contained" onPress={() => _onPress()}>
                    Verify Code from Email
                </Button>
                <TouchableOpacity onPress={()=>console.log('implement it')}>
                    <Text style={styles.text}>Resend Code</Text>
                </TouchableOpacity>
            
            
        </>
    )
}

export default Verify;

const styles = StyleSheet.create({
    textInput:{
      height:50,
      width:300,
      alignSelf:'center'
    },
    text:{
        alignSelf:'center',
        //fontStyle:'italic',
        textDecorationLine:'underline',
        color:'blue',
        textShadowColor:'black',
        textShadowRadius:0.3
    }

});
