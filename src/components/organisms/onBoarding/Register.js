import React, { useEffect } from 'react';
import { Platform,View,StyleSheet,StatusBar} from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Button } from 'react-native-paper';
import {PRIMARY} from '../../../styles/colors'
export default function Register(props) {

    useEffect(()=>{
        
    },[])


  return (
      
    <>
    
        <StatusBar
            backgroundColor={PRIMARY}
            translucent
        />
        <View style={styles.image}>
                {/* <View style={styles.logo}> */}
                    <Animatable.Image 
                        source={require('../../../assets/images/logoWhite.png')} 
                        style={styles.logo}
                        animation="fadeInDownBig"
                    />
                {/* </View> */}
                <View style={styles.void}></View>
                <View style={styles.viewWrapper}>
                    <Button style={{...styles.button,backgroundColor:'black'}} mode="contained" onPress={
                        () => {props.navigation.navigate('LoginStackNavigator')}}>
                        Log In
                    </Button>
                    <Button style={{...styles.button,backgroundColor:'green'}} mode="contained" onPress={() => {props.navigation.navigate('StackNavigator')}}>
                        Register
                    </Button>
                </View>
        </View>
    </>
);
}
const styles = StyleSheet.create({
    area:{
        color:'black',
        alignSelf:'center',
    },
    text:{
        color:'#eff7f6',
        marginBottom:20,
        alignSelf:'center',
        fontSize:30,
        fontStyle:'italic',
        //fontWeight:'bold',
        fontFamily: Platform.OS === 'ios' ?'Noteworthy-Bold':'sans-serif-thin',
    },
    image:{
        alignSelf:'stretch',
        flex: 1,
        backgroundColor:PRIMARY
    },
    viewWrapper:{
        flex:0.2,
        flexDirection:'row',
        justifyContent:'center',
       
    },
    button:{
        height:55,
        width:180,
        textAlign:'center',
        paddingTop:10,
    },
    logo:{
        marginTop:100,
        height:50,
        width:250,
        resizeMode:'contain',
        alignSelf:'center',
        flex:0.3,
       
    },
    logoImage:{
        marginTop:100,
        height:50,
        width:250,
        resizeMode:'contain',
        alignSelf:'center',
        flex:1,
       
    },
    void:{
        flex:1
    },
});