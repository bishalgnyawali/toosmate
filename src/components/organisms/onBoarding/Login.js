import React from 'react';
import {StyleSheet } from 'react-native';
import {TextInput} from 'react-native-paper';
import Content from '../../molecules/Content';
import Button from '../../atoms/OnBoardingButton'

/****************************** */
export default function Login({navigation}) {
  
  return (
    <>
   
      <Content>
      
        <TextInput
                style={styles.textInput}
                placeHolderValue='OTP code'
          />
          <Button style={styles.button} mode="contained" onPress={() => navigation.navigate('PasswordCreate')}>
              Enter OTP Code
          </Button>
      </Content>
      
     
    </>
     
  );
};

const styles = StyleSheet.create({
      
      button:{
        height:40,
        width:300,
        margin:10,
        alignSelf:'center',
        //flexDirection:'column-reverse',
    },
    textInput:{
        height:40,
        width:350,
        alignSelf:'center'
      },
    
});