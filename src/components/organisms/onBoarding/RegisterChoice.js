import React from 'react'
import { Text,StyleSheet, StatusBar} from 'react-native';
import Button from '../../atoms/OnBoardingButton'

import Content from '../../molecules/Content';
const RegisterChoice = ({navigation}) => {
    return (
        <>
         <StatusBar
            backgroundColor={'transparent'}
            translucent
        />

            <Content>
            </Content>
                <Button icon="send" contentStyle={styles.buttonContentStyle} style={styles.button} mode="contained" onPress={() => console.log('Pressed')}>
                    <Text style={styles.text}>SMS OTP Code</Text>
                </Button>
                    
                <Button icon='email' mode="contained" onPress={() => {navigation.navigate('EnterEmail')}}>
                    <Text style={styles.text}>Send Code in Email</Text>
                </Button>
           
                
        </>
    )
}
export default RegisterChoice;
const styles = StyleSheet.create({
    
    buttonContentStyle:{
       
        fontSize:30
    },
    text:{
        color:'white',
    },
 
});

