import React,{useContext}from 'react';
import {StyleSheet,Alert} from 'react-native';
import {TextInput} from 'react-native-paper';

import Content from '../../molecules/Content';
import {getData, loggedIn} from '../../../methods/AsyncStore'
import {LoginContext} from '../../../context/LogInContext'
import Button from '../../atoms/OnBoardingButton'
import ErrorText from '../../atoms/ErrorText'
import axios from '../../../methods/api/index'

import base64 from 'react-native-base64'

const PasswordCreate = ({route,navigation}) => {

    const {register}=route.params;
    console.log('firstname: '+register.fname+'lastname: '+register.lname+'email: '+register.email);
    
    const [login,setLogin]=useContext(LoginContext);

    const[passwordValidate,setPasswordValidate]=React.useState({
        password:'',
        confirm_password:'',
        terms:0,
        error:false,
        errorText:''
    })

    const getLogValue = async ()=>{
        
        await loggedIn('@login','true');
        var value = await getData('@login');
        console.log('value for from value of log in paswordcreate 2 = '+value);
        
    }

    const _onClick=async ()=>{
        // console.log(((passwordValidate.password)===(passwordValidate.confirm_password)) && (passwordValidate.password.length>=8));
        // console.log((passwordValidate.password.length>=8 ));
        if(((passwordValidate.password)===(passwordValidate.confirm_password)) && (passwordValidate.password.length>=8 )){
            //await getLogValue();
            register.password=passwordValidate.password;
            register.confirm_password=passwordValidate.confirm_password;

            axios.post('register-customer', {
                "fname":register.fname,
                "lname":register.lname,
                "email":register.email,
                "password":passwordValidate.password,
                "confirm_password":passwordValidate.confirm_password,
                "terms":"1"
            }).then(function (response){

                // setJsonValue(JSON.parse(base64.decode(response.data.data)));
                // (async ()=>await getLogValue())();
                // (async()=>await loggedIn('@session',response.data.data));
                console.log('response ='+JSON.stringify(response));
                try{
                    console.log('\n\nregister value '+register.data.data);
                    // console.log(JSON.stringify(register));
                    // console.log(base64.encode(JSON.stringify(register)));
                    (async ()=>await loggedIn('@session',response.data.data))();
                    //base64.decode(response.data.data)
                }catch(error){
                    console.log(error);
                }
                // console.log(base64.encode(register));
                // base64.decode(response.data.data)
                // console.log(response);
                console.log('user Created');
                (async ()=>await getLogValue())();
                //(async ()=>await loggedIn('@session',base64.encode(register)))();
                
                Alert.alert(
                    "Logged In",
                    "Your Logged in as Customer, Now you can Post a job",
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ],
                    { cancelable: false }
                );
/***************************************************************************************** */
                    axios.get('customer-current-jobs').then(function (response1){
                        console.log('custome-current-jobs response = '+response1);
                    }).catch(function (error){
                        console.log('\n\n json customer current jobs\n'+JSON.stringify(response1)+'\n\n')
                        console.log('error in customer-current-jobs'+error);
                    })
/***********************************************************************************/              
                setLogin({
                    loginType:false,
                    loginStatus:true
                });

            }).catch(function (error) {
                console.log('\n\nregister value '+JSON.stringify(register));
                console.log('firstname: '+register.fname+'lastname: '+register.lname+'email: '+register.email+' pasword '+passwordValidate.password+' confirm password '+passwordValidate.confirm_password);
                //console.log(JSON.stringify(error));
                setPasswordValidate({
                    ...passwordValidate,
                    errorText:'Either User already exist or Server Error',
                    error:true
                })
            })
            
        }
        else{
            if (passwordValidate.password.length<8){
                setPasswordValidate({
                    ...passwordValidate,
                    errorText:'Password should be minimum 8 characters',
                    error:true
                })
            }
            else{
                setPasswordValidate({
                    ...passwordValidate,
                    errorText:'Password did not match',
                    error:true
                })
            }
       }
        
        
    };
    
    
    return (
        <>
   
            <Content>
            </Content>
                <TextInput
                    style={styles.textInput}
                    //type='outlined'
                    placeholder='Enter New Password'
                    underlineColor='green'
                    secureTextEntry={true}
                    autoCapitalize={'none'}
                    autoCorrect={false}
                    onChangeText={(pass)=>{
                        setPasswordValidate({
                            ...passwordValidate,
                            password:pass,
                            error:false
                        })
                        
                    }}
                />
                <TextInput
                    style={styles.textInput}
                    type='outlined'
                    placeholder='verify password'
                    underlineColor='green'
                    secureTextEntry={true}
                    autoCapitalize={'none'}
                    autoCorrect={false}
                    onChangeText={(cpass)=>{
                        setPasswordValidate({
                            ...passwordValidate,
                            confirm_password:cpass,
                            error:false
                        })
                        
                    }}
                />
                {passwordValidate.error && <ErrorText text={passwordValidate.errorText}/>}
                <Button style={styles.button} mode="contained" onPress={() => _onClick()}>
                    Create Password
                </Button>
        </>
    )
}

export default PasswordCreate;

const styles = StyleSheet.create({
    textInput:{
        height:50,
        width:300,
        alignSelf:'center',
        margin:5
    },
    text:{
        alignSelf:'center',
        //fontStyle:'italic',
        textDecorationLine:'underline',
        color:'blue',
        textShadowColor:'black',
        textShadowOffset: {width: 0.5, height: -0.5},
        textShadowRadius:0.6
    }

});
