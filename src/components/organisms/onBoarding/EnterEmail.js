import React from 'react';
import {StyleSheet, Keyboard} from 'react-native';
import {TextInput } from 'react-native-paper';
import Content from '../../molecules/Content';
import Button from '../../atoms/OnBoardingButton'
import ErrorText from '../../atoms/ErrorText'


const EnterEmail = ({navigation}) => {
    const [text, setText] = React.useState('');
    const [error, setError]=React.useState(false);
    const register=[
        {
            fname:'',
            lname:'',
            email:'',
            password:'',
            confirm_password:'',
            terms:0
        }
    ];
    
    const _verifyEmail=()=>{
        var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
            if(mailformat.test(text)){
                register.email=text;
                console.log(register.email);
                navigation.navigate('Verify',{
                    parameter: register,
                    //caption: 'Verify' 
                })
                
                
                // setValidation({
                //     ...validation,
                //     username: val,
                //     check_textInputChange: true,
                //     isValidUser: true
                // });
            }
            else{
                //console.log(text);
                setError(true);
                
            }
    }
    return (
        <>
            <Content>
            </Content>
            
                
                    <TextInput
                        style={styles.textInput}
                        placeholder='Enter your Email'
                        value={register.email}
                        onChangeText={text=>{
                            setText(text);
                            setError(false);
                        }}
                        //autoCorrect='false'
                        autoCapitalize='none'
                        underlineColor='green'
                        onSubmitEditing={() => Keyboard.dismiss()}
                        autoCorrect={false}
                        //autoCompleteType={'off'}
                        
                    />
                    {error && <ErrorText style={{marginTop:5}} text={'Email address is not valid'}/>}
                    <Button icon="email"
                        onPress={
                            ()=>{
                                // 
                                _verifyEmail();
                            }
                        }
                    >
                        Enter Email and Verify
                    </Button>
                
            
        </>
    )
}

export default EnterEmail;

const styles = StyleSheet.create({
    
    textInput:{
        height:50,
        width:300,
        alignSelf:'center',
    },
});
