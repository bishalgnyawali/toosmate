export const icon=
    [
        { id:"1",title:"Plumbing",Icon:"shower"},
        { id:"2",title:"Painting",Icon:"format-paint"},
        { id:"3",title:"Cleaning",Icon:"spray-bottle"},
        { id:"4",title:"Removal",Icon:"car-pickup"},
        { id:"5",title:"Gardening",Icon:"pine-tree"},
        { id:"6",title:"Land\nScaping",Icon:"shovel"},
        { id:"7",title:"Demolish",Icon:"hammer"},
        { id:"8",title:"Web\nDesigning",Icon:"web"}
    ];

export const popularIconData=
[
    { id:"1",title:"Plumbing",uri:"http://marketplace.toosmate.com/storage/files/au/2765/thumb-816x460-474d4e485f3369e0ae33f1c1b89a3be4.jpg"},
    { id:"2",title:"Painting",uri:"http://marketplace.toosmate.com/storage/files/au/202/thumb-320x240-cee6653ba42218ceb4956a6e583fae06.jpeg"},
    { id:"3",title:"Cleaning",uri:"http://marketplace.toosmate.com/storage/files/au/2773/thumb-320x240-b622f98be31919c4714d9924d7f639db.jpg"},
    { id:"4",title:"Removal",uri:"http://marketplace.toosmate.com/storage/files/au/2546/thumb-320x240-e29e84eb952699de2d3b9de894a39e4d.jpg"},
    { id:"5",title:"Gardening",uri:"http://marketplace.toosmate.com/storage/files/au/2867/thumb-320x240-010a47ae1fd1ff098e535f5b4ebb2286.jpg"},
    { id:"6",title:"Land\nScaping",uri:"http://marketplace.toosmate.com/storage/files/au/1881/thumb-320x240-77940bf1a97f1835a0b68430d5fec356.jpg"},
    { id:"7",title:"Demolish",uri:"https://picsum.photos/200/300"},
    { id:"8",title:"Web\nDesigning",uri:"http://marketplace.toosmate.com/storage/files/au/1080/thumb-320x240-2e7b283eef03fa2560513993fe25b708.jpg"}
];
