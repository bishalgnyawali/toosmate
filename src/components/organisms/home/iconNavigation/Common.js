import React from 'react';
import {StyleSheet,StatusBar, Platform,View} from 'react-native';
import { Appbar} from 'react-native-paper';
import Feather from 'react-native-vector-icons/Feather';

import {PRIMARY} from '../../../../styles/colors'
const Common = (props) => {
    
    return (
        <>
            <StatusBar
                backgroundColor={PRIMARY}
                translucent
            />
            <Appbar.Header style={styles.header}
                statusBarHeight={Platform.OS === "ios" ? 30 : StatusBar.currentHeight}>
                {props.name == "none" || props.name == "nothing"?(Platform.OS!=="ios"?<Appbar.Action style={styles.titleBar}icon="arrow-left"/>:null):null}
                <Appbar.Content style={[styles.titlebar],{fontWeight:'bold'}} title={props.name == "none" || props.name == "nothing"?'':props.name} subtitle="" />
                
                    {props.name=="none"?<Appbar.Action  icon="paperclip"/>:null}
                    {props.name=="none"?<Appbar.Action  icon="delete"/>:null}
                
            </Appbar.Header>
            
        </>
    )
}

export default Common

const styles = StyleSheet.create({
    
    
    item: {
        backgroundColor: "#f9c2ff",
        padding: 20,
        marginVertical: 8
    },
    header: {
        fontSize: 32,
        backgroundColor: PRIMARY,
        flexDirection:'row',
        justifyContent:'space-between'
    },
    title: {
        fontSize: 24
    },
    text:{
        alignSelf:'center',
        fontSize:24,
        marginTop:8,
        //fontFamily:'',
        fontWeight:'bold',
        color:'green'

    }
    
})

