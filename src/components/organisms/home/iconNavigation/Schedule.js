import React,{useEffect,useState} from 'react'
import {View,StyleSheet} from 'react-native'
import {Button as PButton} from 'react-native-paper'

import {PRIMARY} from '../../../../styles/colors'
import DateTimePicker from '@react-native-community/datetimepicker';
import Button from '../../../atoms/Button'


const Schedule = (props) => {
    const [filled, setFilled]=useState(true);
    //const [selectedValue, setSelectedValue] = useState("Select Date");
    useEffect(()=>{
        const unsubscribe = props.navigation.addListener('tabPress', e => {
            // Prevent default behavior
            if(filled) {
                e.preventDefault();
            }
            // Do something manually
            // ...
        });
        
        return unsubscribe;
    },[filled]);

/****************************************************************** */
const [date, setDate] = useState(new Date());
  //const [mode, setMode] = useState('date');
const [show, setShow] = useState(false);

const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
};


/********************************************************************** */
    return (
        <View style={styles.container}>
            
            <PButton style={{margin:20,width:'90%'}} contentStyle={{justifyContent:'flex-start'}} icon="calendar" mode="outlined" labelStyle={{color:'black',alignSelf:'flex-start'}} onPress={() => setShow(!show)}>
                {date.toString().slice(0,15)} {/*{date.getDate()}/{date.getMonth()}/{date.getFullYear()} */}
            </PButton>
            { show &&
                <DateTimePicker
                            testID="dateTimePicker"
                            value={date}
                            mode={'date'}
                            is24Hour={true}
                            display="default"
                            onChange={onChange}
                            minimumDate={new Date()}
                            style={{
                                position:'absolute',
                                bottom:0,
                                right:0,
                                left:0,
                                marginBottom:90
                            }}
                        />
            }   
            
             <Button style={{width:350,borderRadius:30,backgroundColor:PRIMARY}} name={"Continue"} 
                onPress={()=>{
                        setFilled(false);
                        props.navigation.jumpTo('Budget', { owner: 'Bishal' });
                        console.log('Next');
                    }
                }
             />
        </View>
    )
}

export default Schedule

const styles = StyleSheet.create({
    container:{
        flex:1,
        marginTop:30,
        justifyContent:'space-between',
        //flexDirection:'column'
    },
    
})
