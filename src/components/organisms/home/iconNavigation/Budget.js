import React, { useEffect,useState } from 'react'
import { View,StyleSheet,Text,KeyboardAvoidingView} from 'react-native'
import {TextInput} from 'react-native-paper'
//import {windowWidth as width} from '../../../../methods/getWindow';
import Button from '../../../atoms/Button'
import {PRIMARY} from '../../../../styles/colors'
const Budget = (props) => {
    const [filled, setFilled]=useState(true);
    useEffect(()=>{
        
        const unsubscribe = props.navigation.addListener('tabPress', e => {
            // Prevent default behavior
            if(filled) {
                e.preventDefault();
            }
        
            // Do something manually
            // ...
        });
        
        return unsubscribe;
    },[filled]);
    //handle navigation in form fill up part
    return (
        <View style={styles.container}>
    
            <Text>Enter your Price</Text>
            <TextInput
                //value='A$ '
                onSubmitEditing={() => Keyboard.dismiss()}
            />
            <Button style={{width:350,borderRadius:30,backgroundColor:PRIMARY}}
                name={"Confirm"}
                onPress={() => props.navigation.navigate('BottomTabNavigator')}
            />
        </View>
    )
}

export default Budget

const styles = StyleSheet.create({
    container:{
        padding:5,
        flex:1,
    }
})
