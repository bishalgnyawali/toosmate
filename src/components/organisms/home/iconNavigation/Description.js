import React,{useEffect,useState} from 'react'
import { ScrollView,StyleSheet,Text,View} from 'react-native'
import {TextInput} from 'react-native-paper'

import {PRIMARY} from '../../../../styles/colors'
import Button from '../../../atoms/Button'


//import {windowWidth as width} from '../../../../methods/getWindow';
const Description = ({navigation, route}) => {
    const [filled, setFilled]=useState(true);
    const[textJob,setTextJob]=useState('');
    const[textDescription,setTextDescription]=useState('');
    useEffect(()=>{
        const unsubscribe = navigation.addListener('tabPress', e => {
            // Prevent default behavior
            if(filled) {
                e.preventDefault();
            }
        
            // Do something manually
            // ...
        });
        return unsubscribe;
    },[filled]);
    var {title} = route.params
    title=title.replace('\n',' ');
    
    return (
        <>
            <ScrollView style={styles.container}>
                <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                    <Text style={{marginTop:20}}>Job {title}</Text>
                    <Text style={[(textJob.length<10) ? styles.textJobLengthTrue:styles.textJobLengthFalse]}>
                        {textJob.length}/{textJob.length<10?'10+':50}
                    </Text>
                </View>
                <TextInput
                    label={'Do your '+title}
                    value={textJob}
                    onChangeText={textJob => setTextJob(textJob)}
                    type={true}
                    style={styles.textJob}
                    maxLength={50}
                    onSubmitEditing={() => Keyboard.dismiss()}
                    //numberOfLines={2}
                />
                <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                    <Text style={{marginTop:20}}>Job Description</Text>
                    
                    <Text style={[(textDescription.length<25) ? styles.textJobLengthTrue:styles.textJobLengthFalse]}>
                        {textDescription.length}/{textDescription.length<25?'25+':'150'}
                    </Text>
                    
                </View>
                <TextInput
                    style={styles.textStyle}
                    editable
                    multiline={true}
                    onSubmitEditing={() => Keyboard.dismiss()}
                    numberOfLines={20}
                    autoCompleteType={'off'}
                    autoGrow={true}
                    //minLength={80}
                    maxLength={150}
                    value={textDescription}
                    onChangeText={(textDescription)=>setTextDescription(textDescription)}
                />
                <Text>Location of Job</Text>
                <TextInput 
                    style={{marginBottom:100}}
                    onSubmitEditing={() => Keyboard.dismiss()}
                />
                 <Button style={{width:350,borderRadius:30,backgroundColor:PRIMARY}} 
                    name={"Continue"}
                    onPress={()=>{
                        if(textJob.length>=10 && textDescription.length>=25){
                            setFilled(false);
                            navigation.jumpTo('Schedule')
                            console.log('Next');
                        }
                    }}
                />
            </ScrollView>
            {/* <Button style={{width:350,borderRadius:30,backgroundColor:PRIMARY,marginTop:50}} name={"Continue"}/> */}
        </>
    )
}

export default Description

const styles = StyleSheet.create({
    container:{
        padding:10,
        height:'100%',
    },
    textJob:{
        
    },
    textStyle:{
        //height:150,
        minHeight:150,
        maxHeight:200,
        textAlignVertical:"top",
        marginBottom:20,
        //marginTop:20
        
    },
    textJobLengthFalse:{
        marginTop:20,
        
    },
    textJobLengthTrue:{
        marginTop:20,
        color:'red'
    }
})
