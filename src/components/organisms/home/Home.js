import React from 'react'
import { ScrollView ,StyleSheet,Text,SafeAreaView,Platform,View} from 'react-native'
import HomeIconList from '../../molecules/HomeIconList'

import FlatListItem from '../../molecules/FlatListItem'
import Common from './iconNavigation/Common'
import Icon from 'react-native-vector-icons/MaterialIcons'
import {PRIMARY}  from '../../../styles/colors'
const  Home = () => {
    
    //console.log(safeAreaInsets);

    return (
        <>
                <Common name={"Home"}/>
                <SafeAreaView>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        style={styles.scrollViewContainer}
                    >
                        <Text style={styles.text}>Select Jobs To Be Done!</Text>
                        <HomeIconList/>
                        {/* <Text style={{...styles.text,marginTop:50}}>Popular Jobs</Text> */}
                        <View style={styles.instantJobsView}>
                            <View style={styles.iconView}>
                                <Icon style={styles.icon} name='build' size={24} color={PRIMARY}/>
                                <Text style={styles.textPopular}>Instant Jobs</Text>
                            </View>
                            <ScrollView
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                //style={{backgroundColor:'#E5E4DB',borderRadius:10,shadowColor:'black',shadowRadius:5,padding:15}}
                            >  
                                
                                    
                                    <FlatListItem/>
                                
                            </ScrollView>
                        </View> 
                    </ScrollView>
                </SafeAreaView>
                
                
        </>
    )
}

export default Home

const styles = StyleSheet.create({
    
    
    scrollViewContainer:{
        
        marginBottom:Platform.OS === "ios" ? 77:77,
        
    },
    text:{
        alignSelf:'flex-start',
        fontSize:24,
        marginTop:8,
        marginLeft:5,
        //fontFamily:'',
        fontWeight:'bold',
        color:'#111',
    },
    textPopular:{
        fontSize:25,
        fontWeight:'bold',
        paddingBottom:15,
        shadowColor:'black',
        shadowRadius:5,
        shadowOpacity:0.2,
        shadowOffset:{height:-2,width:3},
        marginLeft:20
    },
    instantJobsView:{
        backgroundColor:'#E5E4DB',
        borderRadius:10,
        shadowColor:'black',
        shadowRadius:5,
        borderRadius:10,
        shadowOffset:{widhth:1,height:1},
        paddingBottom:5,
        paddingTop:10,
        elevation:30,
        marginTop:50
    },
    iconView:{
        flexDirection:'row',
        marginTop:5,
        marginLeft:30
    },
    icon:{
        alignItems:'flex-start',
        marginTop:Platform.OS=='ios'?6:8,
    }
    
})
