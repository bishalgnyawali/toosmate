import  React from 'react';
import { StatusBar,FlatList ,SafeAreaView,TouchableOpacity,StyleSheet} from 'react-native'
import msgData from './msgData.json'
import { List } from 'react-native-paper';
import {PRIMARY} from '../../../styles/colors'
function Item({name, message }) {
  return (
    <TouchableOpacity style={styles.item} >
      <List.Item
        title={name}
        description={message}
        titleStyle = {styles.title}
        left={props => <List.Icon {...props}  icon="account-circle" />}
      />
    </TouchableOpacity>
  );
}

export default function MessageList() {
  return (
    <SafeAreaView style={styles.container}>
    <StatusBar backgroundColor= {PRIMARY}/>        
    <FlatList 
        data={msgData.users}
        renderItem={({ item }) => (
          <Item
            id={item.id}
            name={item.firstName}
            message = {item.message}
          />
        )}
      />
    </SafeAreaView>
  );  
}

const styles = StyleSheet.create({
  safeAreaView: {
    marginTop:0,
    flex:1,    
  },
  container: {
    marginTop:10,
    flex: 1,
  },
  item: {
    padding: 0,
    marginVertical: 2,
    marginHorizontal: 10,
  },
  title: {
    fontSize: 15,
  },
})