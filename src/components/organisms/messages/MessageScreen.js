import React from 'react';
import MessageList from '../messages/MessageList'
import Common from '../home/iconNavigation/Common'

//const moreOption = () => console.log('Shown more option');
// const handleSearch = () => console.log('Searching');


export default function MessageScreen() {
  
  return (
    <>
      <Common name="Messages"/>
      <MessageList/>
    </>
  );
};





