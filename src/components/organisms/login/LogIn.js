import React,{useContext,useState} from 'react';
import {StyleSheet,Text,View,Alert} from 'react-native';
import { Button,TextInput} from 'react-native-paper';
import { TouchableOpacity } from 'react-native-gesture-handler';
//import LinearGradient from 'react-native-linear-gradient'
import {LoginContext} from '../../../context/LogInContext';
import {JsonDataContext} from '../../../context/JsonDataContext'

import * as Animatable from 'react-native-animatable';
import {getData,loggedIn} from '../../../methods/AsyncStore'

import {PRIMARY,WHITE} from '../../../styles/colors'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import base64 from 'react-native-base64'
import ErrorText from '../../atoms/ErrorText'
import axios from '../../../methods/api/index'
//import base64 from 'react-native-base64';
/************************************* ******************/

    const LogIn = () => {
        const [jsonValue,setJsonValue]=useContext(JsonDataContext);
        // console.log('userType=='+Jsonvalue)
        const [login,setLogin]=useContext(LoginContext);
        const [error,setError]=useState({
            status: false,
            errorText:''
        });
        const [validation,setValidation]=useState({
            username: '',
            password: '',
            check_textInputChange: false,
            secureTextEntry: true,
            isValidUser: true,
            isValidPassword: true,
        });
        const textInputChange = (val) => {
            var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
            if(mailformat.test(validation.username)){
                setValidation({
                    ...validation,
                    username: val,
                    check_textInputChange: true,
                    isValidUser: true
                });
            }
            else {
                setValidation({
                    ...validation,
                    username: val,
                    check_textInputChange: false,
                    isValidUser: false
                });
            }
        }

        const passwordInputChange=(val)=>{
            setValidation({
                ...validation,
                password:val
            });

        }
    
        const updateSecureTextEntry=()=>{
            setValidation({
                ...validation,
                secureTextEntry:!validation.secureTextEntry
            })
        }
/************************************************* */
    
/*********************************************************************** */
        const getLogValue = async ()=>{

            await loggedIn('@login','true');
            var value = await getData('@login')
            //console.log('value for from value of log in LogIn = ' +value)
        }
/******************************************************** */
        const _onClick=  async()=>{
            //textInputChange(validation.username);
            if(validation.isValidUser && (validation.password.length>=6)){
                //getLogValue();//check for validation
                axios.post('check-customer-login', {
                    
                    "email":validation.username,//"customer@gmail.com",
                    "password":validation.password //"123456"
                    
                }).then(function (response) {
                    //console.log(response.data.data);
                    //console.log(base64.decode(response.data.data));
                    
                    // console.log('@login=== '+JSON.stringify(base64.decode(await getData('@session'))));
                    
                    
                    (async ()=>await getLogValue())();
                    (async()=>await loggedIn('@session',response.data.data));
                    setJsonValue(JSON.parse(base64.decode(response.data.data)));
                    
                    console.log('userType=='+jsonValue.name);
                    // setError({
                    //     errorText:'',
                    //     status:false
                    // });
                    Alert.alert(
                        "Logged In",
                        "Your Logged in as Customer, Now you can Post a job",
                        [
                            { text: "OK", onPress: () => console.log("OK Pressed") }
                        ],
                        { cancelable: false }
                    );
                    setLogin({
                        loginStatus:true,
                        loginType:false
                    })
                
                    
                }).catch(function (error) {
                    // setError({
                    //     errorText:'Email or Password is invalid',
                    //     status:true
                    // });
                    // console.log(error);

                    axios.post('check-tradie-login', {
                        "email":validation.username,//"tradie@gmail.com",
                        "password":validation.password //"123456"
                    }).then(function (response){

                        // Jsonvalue=base64.decode(response.data.data)
                        // console.log('userType=='+Jsonvalue)
                        
                        //console.log(base64.decode(response.data.data));
                        //await loggedIn('@session',response.data.data)
                        // console.log('@login=== '+JSON.stringify(base64.decode(await getData('@session'))));
                        
                        setJsonValue(JSON.parse(base64.decode(response.data.data)));
                        (async ()=>await getLogValue())();
                        (async()=>await loggedIn('@session',response.data.data));
                        console.log('userType=='+jsonValue.name);
                        
                        // setError({
                        //     errorText:'',
                        //     status:false
                        // });
                        
                        Alert.alert(
                            "Logged In",
                            "Your Logged in as Tradie, Now you can Search Task to be done.",
                            [
                                { text: "OK", onPress: () => console.log("OK Pressed") }
                            ],
                            { cancelable: false }
                        );
                        setLogin({
                            loginStatus:true,
                            loginType:true
                        })
                        
                        
                    }).catch(function (error) {
                        setError({
                            errorText:'Email or Password is invalid',
                            status:true
                        });
                        console.log(error);
                    })
                })
            }
            else{
                //textInputChange(validation.username);
                setError({
                    errorText:'Not a valid email or password length',
                    status:true
                })
            }
        };
            
        //console.log('sesion '+JSON.stringify(getData('@session')));

/***************************************************** */
    return (
        <>
            <View style={styles.logoContainer}>
                <Text style={{fontSize:30,fontWeight:'bold',paddingBottom:40}}>Welcome!</Text>
            </View>
            
            <Animatable.View 
                style={styles.componentContainer}
                animation="fadeInUpBig"
            >
            {/* <LinearGradient
                    colors={[PRIMARY]}//
                    style={{...styles.componentContainer,paddingTop:60}}
            > */}
                <View style={styles.textUser}>
                    <FontAwesome 
                        name="user-o"
                        color={WHITE}
                        size={25}
                        style={{marginTop:10}}
                    />
                    <TextInput
                        style={{...styles.textInput}}
                        placeholder='Mobile Number or Email'
                        onChangeText={(val)=>{
                            textInputChange(val)
                            setError({
                                ...error,
                                status:false
                            })
                        }}
                        autoCapitalize="none"
                        autoCompleteType={'off'}
                        //onEndEditing={()=>textInputChange(validation.username)}
                    />
                    {validation.check_textInputChange ? 
                    <Animatable.View
                        animation="bounceIn"
                    >
                        <Feather 
                            name="check-circle"
                            color="pink"
                            size={25}
                            style={{marginTop:10}}
                        />
                    </Animatable.View>
                    : null}
                </View>
                <View style={styles.textUser}>
                    <Feather 
                        name="lock"
                        color={WHITE}
                        size={25}
                        style={{marginTop:10}}
                    />
                    <TextInput
                        style={styles.textInput}
                        placeholder='Password'
                        secureTextEntry={validation.secureTextEntry}
                        autoCapitalize="none"
                        onChangeText={(val)=>{
                            passwordInputChange(val)
                            setError({
                                ...error,
                                status:false
                            })
                        }}
                        autoCompleteType={'off'}
                        //onEndEditing={()=>textInputChange(validation.username)}
                    />
                    <TouchableOpacity
                        onPress={updateSecureTextEntry}
                    >
                    {validation.secureTextEntry ? 
                        <Feather 
                            name="eye-off"
                            color={WHITE}
                            size={25}
                            style={{marginTop:10}}
                        />
                        :
                        <Feather 
                            name="eye"
                            color={WHITE}
                            size={25}
                            style={{marginTop:10}}

                        />
                    }
                </TouchableOpacity>
                </View>
                {error.status && <ErrorText text={error.errorText}/>}
                <Button style={styles.button} mode="outlined" onPress={() =>
                    //textInputChange(validation.username);
                    _onClick()
                }
                    compact={false}
                >
                    LogIn
                </Button>
                
                <TouchableOpacity onPress={()=>{console.log('forgot password pressed')}}>
                    <Text style={styles.text}>Forgot Password</Text>
                </TouchableOpacity>
                {/* </LinearGradient> */}
            </Animatable.View>
            
            
        </>
    )
}

export default LogIn;

const styles = StyleSheet.create({
    textUser:{
        flexDirection:'row',
        alignContent:'center',
        paddingLeft:40
    },
    textInput:{
        height:45,
        width:265,
        borderWidth:1,
        borderRadius:5,
        marginLeft:5,
        marginRight:5,
        marginBottom:10
      },
    logoContainer:{
        flex:1,
        backgroundColor:'#eee',
        alignContent:'center',
        flexDirection:'column-reverse',
        alignItems:'flex-start',
        paddingLeft:30
    },
    componentContainer:{
        flex:3,
        backgroundColor:PRIMARY,
        borderTopLeftRadius:30,
        borderTopRightRadius:30,
        paddingTop:60
    },
    button:{
      height:45,
      width:300,
      margin:10,
      borderRadius:20,
      justifyContent:'center',
      alignSelf:'center',
      backgroundColor:WHITE
    },
    
    text:{
        alignSelf:'center',
        textDecorationLine:'underline',
        color:'blue',
        textShadowColor:'black',
        textShadowOffset: {width: 0.5, height: -0.5},
        textShadowRadius:0.6
    }

});
