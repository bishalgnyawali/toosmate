import React,{useContext} from 'react'
import { ScrollView,View, Text,StyleSheet } from 'react-native'
import Common from '../home/iconNavigation/Common';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Feather from 'react-native-vector-icons/Feather';

import {LoginContext} from '../../../context/LogInContext'
import {removeValue} from '../../../methods/AsyncStore'
//import SettingsNavigator from '../../../navigations/SettingsNavigator';
import { useNavigation } from '@react-navigation/native';

const Settings = (props) => {

  const navigation = useNavigation();

  const [login,setLogin]= useContext(LoginContext);

    /******************************************* */
    const moreOption = async(value) => {
        if(value=='logOut'){
          
          
          await removeValue('@session');
          await removeValue('@login');
        
          setLogin({
            ...login,
            loginStatus:false
          });
          
          
        }
        else if (value=='paymentOptions'){
          navigation.navigate('PaymentOptionsTabNavigator');
        }
        else if (value=='aboutToosMate'){
          navigation.navigate('About');
        }
        else if (value=='notification'){
          navigation.navigate('Notifications');
        }
        else if (value=='help'){
          navigation.navigate('Help');
        }
        else if (value=='myProfile'){
          navigation.navigate('Profile');
        }
      
    }
  const items=[
    {label:'My Profile',value:'myProfile'},{label:'About ToosMate',value:'aboutToosMate'},{label:'Notification',value:'notification'},{label:'Payment Options',value:'paymentOptions'},{label:'Help',value:'help'},{label:'Log Out',value:'logOut'}
  ];
    return(
        <>
          <Common name="Settings"/>
          <ScrollView style={styles.container}>
            {
              items.map((item,key) => (
                <TouchableOpacity key={key} 
                  onPress={() => 
                    moreOption(item.value)
                  }
                >
                  <View style={styles.itemContainer}>
                      <Text style={styles.text}>{item.label}</Text>
                      <Feather
                        name="more-horizontal"
                        //color={WHITE}
                        size={25}
                        style={{}}
                      />
                  </View>
                </TouchableOpacity>
              ))
            }
          </ScrollView>
          
        </>
    );
}

export default Settings

const styles = StyleSheet.create({
  container:{
    flex:1,
  },
  itemContainer:{
    padding:10,
    backgroundColor:'#fafafa',
    flexDirection:'row',
    justifyContent:'space-between',
    height:60,
    borderWidth:0,
    borderColor:'black',
    shadowColor:'black'
  },
  text:{
    fontSize:18,
    fontWeight:'bold',
    color:'grey'

  }
})

