import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import Common from '../../home/iconNavigation/Common'
import { Avatar } from 'react-native-paper';
const MyProfile = () => {
    return (
        <>
            <Common name="nothing"/>
            <View style={styles.container}>
                <Avatar.Image style={styles.avatar} size={210} source={{uri: 'https://toosmate.prithak.com.np/images/profile/SIUqFmfkyR-2020-09-15_08-10-22.jpg'}} />
            </View>
        </>
    )
}

export default MyProfile
const styles = StyleSheet.create({
    container:{
        margin:10
    },
    avatar:{
        alignSelf:'center'
    }
})
