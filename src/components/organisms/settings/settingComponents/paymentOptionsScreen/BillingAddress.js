import React from 'react'
import { View, Text,TextInput } from 'react-native'
import Button from '../../../../atoms/Button'
import {PRIMARY} from '../../../../../styles/colors';
const BillingAddress = (props) => {
    return (
        <>
            <View>
                <Text>Address line 1</Text>
                <TextInput></TextInput>
                <Text>Address line 2</Text>
                <TextInput></TextInput>
                <Text>Suburb</Text>
                <TextInput></TextInput>
                <Text>State</Text>
                <TextInput></TextInput>
                <Text>Post Code</Text>
                <TextInput></TextInput>
                <Text>Country</Text>
                <TextInput></TextInput>
            </View>
            <Button style={{width:350,borderRadius:30,backgroundColor:PRIMARY}} name={"Save"}
                    onPress={() => props.navigation.navigate('BottomTabNavigator')}
            />  
        </>
    )
}

export default BillingAddress