import React,{useState} from 'react'
import { View, Text, StyleSheet,ScrollView } from 'react-native'
import { TextInput } from 'react-native-paper';
import Button from '../../../../atoms/Button'
import { PRIMARY } from '../../../../../styles/colors';



const PaymentOptionsScreen = (props) => {

    const [card, setCard]=useState({
        firstName: '',
        surName: '',
        creditCardNumber: '',
        expiryMonth: '',
        year: '',
        cardCvc: '',
        set: false
    });

    return (
        <View style={styles.container}>
            <ScrollView>
                    <View style={styles.nameContainer}>
                        <Text>
                            First Name
                        </Text>
                        <Text>
                            Surname
                        </Text>
                    </View>
                    <View style={styles.nameContainer}>
                        <TextInput
                            onSubmitEditing={() => Keyboard.dismiss()}
                        >

                        </TextInput>
                        
                        <TextInput
                            onSubmitEditing={() => Keyboard.dismiss()}
                        >
                            
                        </TextInput>
                    </View>
                    <View>
                        <Text>
                            Credit Card Number
                        </Text>
                        <TextInput
                            onSubmitEditing={() => Keyboard.dismiss()}
                        >
                        </TextInput>
                    
                    </View>
                    <View style={styles.nameContainer}>
                        <Text>
                            Expiry
                        </Text>
                        <Text>
                            CVC
                        </Text>
                    </View>
                    <View style={styles.nameContainer}>
                        <TextInput
                            onSubmitEditing={() => Keyboard.dismiss()}
                        >
                            Month
                        </TextInput>
                        <TextInput
                            onSubmitEditing={() => Keyboard.dismiss()}
                        >
                            
                        </TextInput>
                        
                        <TextInput
                            onSubmitEditing={() => Keyboard.dismiss()}
                        >
                            
                        </TextInput>
                    </View>
                    <View style={styles.termsAndConditions}>
                        <Text>By providing your credit card details, you agree to Stripe's </Text>
                        <Text style={{color:'grey'}}>
                                Terms & Conditions.
                        </Text>
                    </View>
                </ScrollView>
                <Button style={{width:350,borderRadius:30,backgroundColor:PRIMARY}} name={"Save"}
                    onPress={() => props.navigation.navigate('BottomTabNavigator')}
                />
        </View>
    )
}

export default PaymentOptionsScreen;

const styles = StyleSheet.create({
    container:{
        flex:1,
        padding:10
    },
    nameContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        
    },
    termsAndConditions:{
        
    }
})
