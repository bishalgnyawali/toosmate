import React from 'react';
//import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';


import PaymentOptionsScreen from '../settingComponents/paymentOptionsScreen/PaymentOptionsScreen'
import BillingAddress from '../settingComponents/paymentOptionsScreen/BillingAddress'
//import { HeaderTitle } from '@react-navigation/stack';
import Common from '../../../organisms/home/iconNavigation/Common'
import {PRIMARY,WHITE} from '../../../../styles/colors';



export default function MyTaskTopTabNavigator() {

    const Tab = createMaterialTopTabNavigator();
    
    //const insets = useSafeArea();
    //useEf

    return (
        <>
            <Common name="My PaymentOptionsScreen"/>
            <Tab.Navigator 
                tabBarOptions={{
                    activeTintColor: WHITE,
                    tabStyle:{
                        backgroundColor:PRIMARY,
                    },
                    style: {
                        // marginTop: insets.top+40,
                        borderTopWidth: 0,
                        //borderTopColor: "transparent",
                    }
                }}
                //swipeEnabled={false}
                //backBehavior={"initialRoute"}
            >
                <Tab.Screen name="DEBIT" component={PaymentOptionsScreen}/>
                <Tab.Screen name="CREDIT" component={BillingAddress}/>
                
            </Tab.Navigator>
        </>
    );
}
