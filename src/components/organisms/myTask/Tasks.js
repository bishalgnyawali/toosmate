import React, { useState } from 'react'
import { View, Text } from 'react-native'
import DropDownPicker from 'react-native-dropdown-picker';
//import Icon from 'react-native-vector-icons/Feather';
const Tasks = () => {
    const[filter,setFilter]=useState();
    return (
        <View>
            {/* <Text>Will load from api Tasks</Text> */}
            <DropDownPicker
                items={[
                    {label:'All Tasks',value:'all tasks',selected: true},{label:'Posted',value:'posted'},{label:'Draft',value:'Draft'},{label:'Canceled',value:'canceled'}
                ]}
                // items={[{label: 'UK', value: 'uk', icon: () => <Icon name="flag" size={18} color="#900" />},
                //         {label: 'France', value: 'france', icon: () => <Icon name="flag" size={18} color="#900" />},
                // ]}
                //defaultValue={'filter'}
                containerStyle={{height: 40,marginTop:10}}
                style={{backgroundColor: '#fafafa',alignSelf:'flex-start',width:'50%',marginLeft:10}}
                itemStyle={{
                    justifyContent: 'flex-start',
                }}
                //onChangeList={(items)=>{setFilter(items)}}
                dropDownStyle={{
                    backgroundColor: '#fafafa',
                    alignSelf:'flex-start',
                    width:'50%',
                    marginLeft:10,
                    marginTop:2
                }}
                onChangeItem={items => setFilter(items)}
            />

        </View>
    )
}

export default Tasks
