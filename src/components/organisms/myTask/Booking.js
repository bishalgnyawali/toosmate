import React,{useState} from 'react'
import { View, Text } from 'react-native'
import DropDownPicker from 'react-native-dropdown-picker';
import Icon from 'react-native-vector-icons/Feather';
const Booking = () => {
    const[filter,setFilter]=useState();
    return (
        <View>
        {/* <Text>Will load from api Tasks</Text> */}
            <DropDownPicker
                //items={['As a Poster','As a Tasker']}
                items={[
                    {label: 'As a Poster', value: 'item1'},
                    {label: 'As a tasker', value: 'item2', selected: true},
                ]}
                
                //multiple={true}
                //multipleText={}
                min={0}
                max={10}
                //defaultValue={filter}
                containerStyle={{height: 40,marginTop:10}}
                style={{backgroundColor: '#fafafa',alignSelf:'flex-start',width:'50%',marginLeft:10}}
                itemStyle={{
                    justifyContent: 'flex-start',
                }}
                dropDownStyle={{
                    backgroundColor: '#fafafa',
                    alignSelf:'flex-start',
                    width:'50%',
                    marginLeft:10,
                    marginTop:2
                }}
                onChangeItem={items => setFilter(items)}
            />

    </View>
    )
}

export default Booking
