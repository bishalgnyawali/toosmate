import  React from 'react';
import { SafeAreaView, FlatList, TouchableOpacity,StyleSheet} from 'react-native'
import searchData from './searchData.json'
import {List } from 'react-native-paper';
import SearchPosts from '../search/SearchPosts.js'
//import {getData,loggedIn} from '../../../methods/AsyncStore'
import Common from '../home/iconNavigation/Common'
//import {LoginContext} from '../../../context/LogInContext'

function Item({name, message }) {
  return (
    <TouchableOpacity style={styles.item} >
      <List.Item
        title={name}
        description={message}
        titleStyle = {styles.title}
        right ={props => <List.Icon {...props}  icon="account-circle" />}
      />
    </TouchableOpacity>
  );
}

//const moreOption = () => console.log('Shown more option');
export default function SearchScreen() {
  
  return (
    <>   
      <Common name="Search"/>
      
      <SafeAreaView style={styles.container}>
        <SearchPosts>
        </SearchPosts>
        <FlatList 
          data={searchData.search}
          renderItem={({ item }) => (
            <Item
              id={item.id}
              name={item.firstName}
              message = {item.message}
            />
          )}
        />
      </SafeAreaView>
    </>
  );  
}
const styles = StyleSheet.create({
  // item: {
  //   backgroundColor: 'white',
  //   padding: 0,
  //   marginVertical: 3,
  //   marginHorizontal: 5,
  // },
  title: {
    fontSize: 20,
  },
});

