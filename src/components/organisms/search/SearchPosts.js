import * as React from 'react';
import { Searchbar } from 'react-native-paper';
import { StyleSheet } from 'react-native';

const SearchPost= () => {
    const [searchQuery, setSearchQuery] = React.useState('');
  
    const onChangeSearch = query => setSearchQuery(query);
  
    return (
      <Searchbar 
        style = { styles.textInput}
        placeholder="Search"
        onChangeText={onChangeSearch}
        value={searchQuery}
      />
    );
  
  
  };
  export default SearchPost;

const styles = StyleSheet.create({
    textInput:{
          marginTop:10,
          fontWeight: "bold",
          alignSelf:'center',
          margin:10,
      },
  });
  