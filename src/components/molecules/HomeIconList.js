import React from 'react'
import { View, Text,StyleSheet,TouchableOpacity} from 'react-native'
import { Avatar} from 'react-native-paper';
import {icon} from '../organisms/home/iconData'

import { useNavigation } from '@react-navigation/native';
import{ICON_COLOR} from '../../styles/colors'



const HomeIconList = (props) => {


    const navigation = useNavigation();
    
    return (
        <View style={styles.view}>
            
            {
                icon.map((item,key) => (
                    <TouchableOpacity
                        onPress={() => {
                                navigation.navigate('IconStackNavigator',{screen:'Description',params:{title:item.title},});
                                console.log(item.title);
                            }
                        }
                        key={key}
                    >
                        <View style={styles.iconView}>
                            <Avatar.Icon size={60} icon={item.Icon} style={styles.avatar} key={item.id}/>
                            
                                <Text style={styles.text}>
                                    {item.title}
                                </Text>  
                            
                        </View>
                    </TouchableOpacity>
                ))
            }   
            
        </View>
    )
}

export default HomeIconList

const styles = StyleSheet.create({
    avatar:{
      backgroundColor:ICON_COLOR,
    },
    iconView:{
        flexDirection:'column',
        alignItems:'center',
        marginLeft:30,
        marginTop:15,
        elevation:20
    },
    view:{
        flex:1, 
        flexDirection:'row',
        flexWrap:'wrap',
        backgroundColor:'#E5E4DB',
        borderRadius:10,
        shadowColor:'black',
        shadowRadius:5,
        shadowOffset:{widhth:1,height:1},
        shadowRadius:5,
        paddingBottom:15,
        elevation:30
    },
    text:{
        fontSize:12,
        fontWeight:'bold',
        paddingTop:5,
        textAlign:'center'
    }
})
