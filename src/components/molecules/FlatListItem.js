import React from 'react'
import { View,StyleSheet } from 'react-native'


import {popularIconData} from '../organisms/home/iconData'
import PopularCard from '../atoms/PopularCard'
import { TouchableOpacity } from 'react-native-gesture-handler';

const FlatListItem = () => {


    return (
        <View style={styles.view}>
            {popularIconData.map((item,key)=>(
                <TouchableOpacity key={key} onPress={()=>console.log(item.title+" is pressed")}>
                    <PopularCard title={item.title.replace('\n',' ')} uri={item.uri} key={key}/>
                </TouchableOpacity>

                )

            )}
        </View>
    )
}

export default FlatListItem;

const styles = StyleSheet.create({
    view:{
        flex:1,
        flexDirection:'row',
        marginBottom:15,
        marginLeft:5,
        elevation:20
    }
})



