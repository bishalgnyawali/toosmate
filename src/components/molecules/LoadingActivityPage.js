import React from 'react'
import { View,ActivityIndicator,StyleSheet } from 'react-native'

import {PRIMARY,WHITE} from '../../styles/colors'

const LoadingActivityPage = () => {
    return (
        <View style={styles.container}>
                {/* <Text>try to make loading page with some logo and message</Text> */}
                <ActivityIndicator size="large" color={WHITE} />
        </View>
    )
}

export default LoadingActivityPage

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        backgroundColor:PRIMARY
    }
})

