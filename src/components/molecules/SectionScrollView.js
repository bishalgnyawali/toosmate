import React from 'react'
import { 
    Text,
    SafeAreaView,
    SectionList,
    StyleSheet,
    View,
    StatusBar
} from 'react-native'
import { Appbar} from 'react-native-paper';
import {PRIMARY,WHITE} from '../../styles/colors'

const DATA = [
    {
      title: "Main dishes",
      data: ["Pizza", "Burger", "Risotto"]
    },
    {
      title: "Sides",
      data: ["French Fries", "Onion Rings", "Fried Shrimps"]
    },
    {
      title: "Drinks",
      data: ["Water", "Coke", "Beer"]
    },
    {
      title: "Desserts",
      data: ["Cheese Cake", "Ice Cream"]
    }
  ];

  const Item = ({ title }) => (
    <View style={styles.item}>
      <Text style={styles.title}>{title}</Text>
    </View>
  );

  const moreOption = () => console.log('Shown more option');
const SectionScrollView = ({item}) => {
    return (
      <>
         <StatusBar
          backgroundColor={'transparent'}
          translucent
        />
        <Appbar.Header style={styles.header}
          statusBarHeight={Platform.OS === "ios" ? 30 : StatusBar.currentHeight}>
              <Appbar.Content style={styles.titlebar} title="Home" subtitle="" />
              <Appbar.Action  />
              <Appbar.Action icon="dots-vertical" onPress={moreOption} />

        </Appbar.Header>
        
        <SafeAreaView style={styles.container}>
            <SectionList
                sections={DATA}
                keyExtractor={(item, index) => item + index}
                renderItem={({ item }) => <Item title={item} />}
                renderSectionHeader={({ section: { title } }) => (
                            <Text style={{...styles.header, backgroundColor:WHITE}}>{title}</Text>
                )}
            />
        </SafeAreaView>
      </>
    )
}

export default SectionScrollView

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 15,
        marginHorizontal: 16,
      },
      item: {
        backgroundColor: "#f9c2ff",
        padding: 20,
        marginVertical: 8
      },
      header: {
        fontSize: 32,
        backgroundColor: PRIMARY,//"#fff"
      },
      title: {
        fontSize: 24
      }
})
