import React from 'react'
import { View,StatusBar,ScrollView,SafeAreaView,StyleSheet, Image} from 'react-native'
import {Avatar} from 'react-native-paper';
const Content = (props) => {
    return (
        <>
            <SafeAreaView style={styles.safeAreaView}>
                    <Image style={styles.logo} source={require('../../assets/images/toosMateLogo.png')} />
                <ScrollView style={styles.scrollView}>
                    {props.children}
                </ScrollView>
            </SafeAreaView>
        </>
    )
}

export default Content
const styles = StyleSheet.create({
    safeAreaView: {
        flex:0.4,    
    },
    logo:{
        //flexDirection:'column-reverse',
        flex:1,
        maxHeight:70,
        marginTop:100,
        alignSelf:'center',
        resizeMode:'contain',
    },
})