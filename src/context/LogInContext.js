import React, {createContext,useState, useEffect } from 'react'
import {getData} from '../methods/AsyncStore'


export const LoginContext = createContext();

export const LoginProvider= (props)=>{
    //var value = (async ()=>await getData('@login'))=='true'?true:false;
    //const [bool,setBool]=useState();
    const [login,setLogin]=useState({
        loginStatus:(async()=>await getLogValue()),
        loginType:Boolean// true for tradie false for customer type
    });
    async function getLogValue() {
        
        var value = await getData('@login');
        console.log('value for from getData in context= '+value)
        if(value == 'false'){
            setLogin({
                ...login,
                loginStatus:false
            })
            //setBool(false);
            return false;
        }else if(value=='true'){
            setLogin({
                ...login,
                loginStatus:true
            })
            //console.log('value===== '+value);
            //setBool(true);
            return true;
        }else{
            setLogin({
                ...login,
                loginStatus:false
            })
            //setBool(false);
            return false;
        }
    }
    
    //getLogValue().then((value)=>console.log('logbool==== '+bool));
    

    useEffect(()=>{
        // const val =async ()=>await getLogValue();
        // //getLogValue().then((value)=>console.log('logbool==== '+bool));
        // val();
        (async()=>
            setLogin({
                ...login,
                loginStatus:await getLogValue()
            })
        )();
        // setLogin({
        //     ...login,
        //     loginStatus:bool,
        //     //loginType:true//tradie
        // });
    },[]);

    

    return(
        <LoginContext.Provider value={[login,setLogin]}>
            {props.children}
        </LoginContext.Provider>
    );
}

export const LoginConsumer=LoginContext.Consumer



