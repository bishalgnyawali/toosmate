import React, {createContext,useState, useEffect } from 'react'
import {getData} from '../methods/AsyncStore'
import base64 from 'react-native-base64'

export const JsonDataContext = createContext();

export const JsonDataProvider=(props)=>{
    const [jsonValue,setJsonValue]=useState({});
    const getLogValue = async ()=>{
        //await loggedIn('true');
        value = base64.decode(await getData('@session'));
        setJsonValue(value);
        console.log('value for from getData in context= '+value)
        
    }
    //getLogValue();
    useEffect(()=>{
            (async ()=>await getLogValue());
        },[])



    return(
        <JsonDataContext.Provider value={[jsonValue,setJsonValue]}>
            {props.children}
        </JsonDataContext.Provider>
    );
}

export const JsonDataConsumer=JsonDataContext.Consumer



