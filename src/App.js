/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { NavigationContainer } from '@react-navigation/native';

import InitialStackNavigator from './navigations/InitialStackNavigator'
import {LoginProvider} from './context/LogInContext'
import {JsonDataProvider} from './context/JsonDataContext'

//import {getData,loggedIn} from './methods/AsyncStore';

    


const App: ()=>React$Node = () => {

  
  return (
      <LoginProvider>
        <JsonDataProvider>
          <NavigationContainer>
              <InitialStackNavigator/>
          </NavigationContainer>
         </JsonDataProvider>
      </LoginProvider>
  )
}



export default App;
