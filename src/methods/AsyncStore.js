
import AsyncStorage from '@react-native-community/async-storage';

export const loggedIn = async (key,value) => {
    try {
      await AsyncStorage.setItem(key,value)
    } catch (e) {
      // saving error
    }
  }


export const getData = async (key) => {
    try {
      const value = await AsyncStorage.getItem(key)
      if(value !== null) {
        // value previously stored
        return value;
      }
    } catch(e) {
      // error reading value
    }
  }


  export const removeValue = async (key) => {
    try {
      await AsyncStorage.removeItem(key)
    } catch(e) {
      // remove error
    }
  
    console.log('Done.')
  }

  // export const asyncSessionData = async (somedata)=>{
  //   try
  // }

  


  